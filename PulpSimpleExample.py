import pandas as pd
from pulp import *

''' SET HIGH LEVEL CONSTRAINTS '''
OPTSET_MILE_LIMIT_UPPER = 22000
OPTSET_MILE_LIMIT_LOWER = 20000


'''
Import the data, set the laneId as the Index
'''
data = pd.read_excel('SampleData.xlsx')


''' INIT LP SOLVER '''
prob = pulp.LpProblem('LaneSelectionOptimization', LpMaximize)

''' SOLVER SETUP  '''
Lanes = data.index
MaxVols = data['MaxVol']
MinVols = data['MinVol']
Impacts = data['ImpactPer']
Miles = data['Distance']

x = LpVariable.dicts('Lane', Lanes, None, None, LpInteger)

for l in Lanes:
    x[l].bounds(MinVols[l], MaxVols[l])
    
''' SET THE OBJECTIVE FUNCTION '''
prob += sum([x[l] * Impacts[l] for l in Lanes]), 'Sum_of_Impact'


''' SET THE CONSTRAINTS '''
prob += lpSum([Miles[l] * x[l] for l in Lanes]) <= OPTSET_MILE_LIMIT_UPPER
prob += lpSum([Miles[l] * x[l] for l in Lanes]) >= OPTSET_MILE_LIMIT_LOWER


'''WRAP UP AND SOLVE '''
LpSolverDefault.msg = 1
prob.writeLP('LaneOpt.lp')
prob.solve()


''' HANDLE RESULTS '''
TotalImpact = prob.objective.value()
Status = pulp.LpStatus[prob.status]
print('Status:',Status)
print('Total Impact:', TotalImpact)

flow = {l:x[l].varValue for l in Lanes}

output = []
for lane in Lanes:
    var_output = {
            'Lane':lane,
            'Production':flow[lane]
            }
    output.append(var_output)
    
dfOptResults = pd.DataFrame.from_records(output)
dfOptResults.set_index('Lane', inplace=True)

''' MERGE RESULTS WITH ORIGINAL DATA '''
data = pd.merge(data, dfOptResults, how='left', left_index=True, right_index=True)

''' BONUS: CALC IMPACT FOR EACH LANE '''
data['LaneImpact'] = data['ImpactPer'] * data['Production']